Role Name
=========

This role sets up haproxy with a basic configuration

Requirements
------------

No requirements

Role Variables
--------------

```
haproxy_user: haproxy
haproxy_group: haproxy
haproxy_retries: 2
haproxy_connection_timeout: 3000
haproxy_client_timeout: 5000
haproxy_server_timeout: 5000
haproxy_packages:
  redhat:
    - haproxy
haproxy_services:
  - service_name: web
    enable_ssl: False
    ssl_cert_path: /etc/haproxy/ssl.pem
    bind_ip: 127.0.0.1
    proto: tcp
    port: 81
    check_port: 81
    health_check_fall: 3
    health_check_rise: 2
    balancer_mode: leastconn
    backend_hosts:
      - hostname: web01
        ip_addr: x.x.x.x
      - hostname: web02
        ip_addr: x.x.x.x
```

Dependencies
------------

```
roles:
  - ansible-role-packages
```

Example Playbook
----------------

```
    - hosts: haproxy
      roles:
         -  role: ansible_role_haproxy
            haproxy_services:
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
