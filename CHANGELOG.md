# 1.0.0 (2020-02-20)


### Features

* Add the ability to turn on stats interface ([ba17bfa](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/commit/ba17bfa)), closes [#5](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/issues/5)
